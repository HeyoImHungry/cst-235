//Noah Canepa
package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import beans.User;

@ManagedBean
public class FormController {
	public String onSubmit() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		//shows data in console to make sure it's working properly
		System.out.println(user.getFirstName());
		System.out.println(user.getLastName());
		System.out.println("test");
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Response.xhtml";
	}
}
