package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;

@Stateless
@Local(OrderBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrderBusinessInterface {
	List<Order> orders = new ArrayList<Order>();
	
	@Override
	public void test() {
		System.out.println("This Aplication is Running");
	}
	
	public OrdersBusinessService() {
		 
		 orders.add(new Order("0000000000", "this is product 1", (float)1.00, 1));
		 orders.add(new Order("0000000001", "this is product 2", (float)1.10, 1));
		 orders.add(new Order("0000000002", "this is product 3", (float)1.20, 1));
		 orders.add(new Order("0000000003", "this is product 4", (float)1.30, 1));
		 orders.add(new Order("0000000004", "this is product 5", (float)1.40, 1));
		 orders.add(new Order("0000000005", "this is product 6", (float)1.50, 1));
		 orders.add(new Order("0000000006", "this is product 7", (float)1.60, 1));
		 orders.add(new Order("0000000007", "this is product 8", (float)1.70, 1));
		 orders.add(new Order("0000000008", "this is product 9", (float)1.80, 1));
		 orders.add(new Order("0000000009", "this is product 10", (float)9.00, 1));
	}

	@Override
	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		return orders;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orders = orders;
		
	}

}
