package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;

@Stateless
@Local(OrderBusinessInterface.class)
@Alternative
public class AnotherOrdersBusinessService implements OrderBusinessInterface {
	
	List<Order> orders = new ArrayList<Order>();
	
	@Override
	public void test() {
		System.out.println("Another Aplication is Running");
	}
	
	public AnotherOrdersBusinessService() {
		 
		 orders.add(new Order("000000000a", "this is Another product 1", (float)2.00, 10));
		 orders.add(new Order("000000000b", "this is Another product 2", (float)2.10, 10));
		 orders.add(new Order("000000000c", "this is Another product 3", (float)2.20, 10));
		 orders.add(new Order("000000000d", "this is Another product 4", (float)2.30, 10));
		 orders.add(new Order("000000000e", "this is Another product 5", (float)2.40, 10));
		 orders.add(new Order("000000000f", "this is Another product 6", (float)2.50, 10));
		 orders.add(new Order("000000000g", "this is Another product 7", (float)2.60, 10));
		 orders.add(new Order("000000000h", "this is Another product 8", (float)2.70, 10));
		 orders.add(new Order("000000000i", "this is Another product 9", (float)2.80, 10));
		 orders.add(new Order("000000000j", "this is (I wanted this to be different) product 10", (float)99.00, 1));
	}

	@Override
	public List<Order> getOrders() {
		// TODO Auto-generated method stub
		return orders;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orders = orders;
		
	}

}
