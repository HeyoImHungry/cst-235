//Noah Canepa
package controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.User;
import business.MyTimerService;
import business.OrderBusinessInterface;

@ManagedBean
public class FormController {
	
	@Inject
	OrderBusinessInterface services;
	
	@EJB
	MyTimerService timer;
	
	public String onSubmit() {
		//retrieves user values
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);
		
		//prints message to console to tell which Business class is currently selected as an alternative
		services.test();
		
		//start a timer when login is clicked
		timer.setTimer(10000);
		
		//put user into Post request
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		//show next page
		return "Response.xhtml";
	}
	
	public OrderBusinessInterface getService() {
		return services;
	}
}
